# Homepage



## Introduction

Homepage is a simple script intended to replace your browser's New Tab page.

`index.html` is a husk that contains the bare minimum required to load `style.css` and `main.js`, which in turn checks `sites.js` for a list of your desired shortcuts. That's it, nothing fancy. Icons are cached (per domain) in local storage to make loading near instant.

## Installation

Download the source to its own directory, set up your `sites.js` file (an example is included), and open `index.html` with your browser. Once you have verified it's working, set that location as your browser's homepage. Alternatively, you could run a webserver on your computer or a networked server and access it from there. This script has no dependencies.

If you would like Homepage as your New Tab page there are extensions that can allow you to clear and autoselect the address bar just like the default does. [Custom New Tab Page](https://addons.mozilla.org/en-US/firefox/addon/custom-new-tab-page/) for Firefox is an excellent example.

## Customization

The included `style.css` is very simple and has some variables at the top for quick tuning.
