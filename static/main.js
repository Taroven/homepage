import {sites, categories} from './sites.js';

function getFavIcon (url) {
	let gstatic = 'https://t3.gstatic.com/faviconV2?client=SOCIAL&type=FAVICON&fallback_opts=TYPE,SIZE,URL&url=';
	let size = '&size=64';
	let domain = (new URL(url));
	let origin = domain.origin;

	let fav = gstatic + origin + size;

	// Check if the favicon is stored in local storage
	let cachedFavicon = localStorage.getItem(fav);
	if (cachedFavicon) {
		// console.log('Using cached icon for ' + fav)
		return cachedFavicon;
	} else {
		// Load the favicon from the server
		let image = new Image();
		image.src = fav;
		image.onload = function() {
			// Store the favicon in local storage
			localStorage.setItem(fav, image.src);
		};
		return fav;
	}
}

function getLink (name, url, img, opt) {
	let anchor = document.createElement('a');
	anchor.href = url;
	if (img) {
		let iconUrl = ((opt == undefined) ? url : opt);
		let element = document.createElement('img');
		let fav = getFavIcon(iconUrl);
		element.setAttribute('src', fav);
		element.setAttribute('alt', name);
		element.setAttribute('width', '64px');
		anchor.appendChild(element);
	} else {
		let link = document.createTextNode(name);
		anchor.appendChild(link);
	}
	return anchor;
}

function buildCell (name, url, opt) {
	let img = getLink(name, url, true, opt);
	let txt = getLink(name, url);
	
	let div = document.createElement('a');
	div.setAttribute('class', 'grid-item');
	div.href = url;
	
	let span = document.createElement('span');
	span.setAttribute('class', 'caption');
	span.appendChild(txt);

	div.appendChild(img);
	div.appendChild(span);

	return div;
}

function buildGrid (entries) {
	let grid = document.createElement('div');
	grid.setAttribute('class', 'grid-container');

	for (let i = 0; i < entries.length; i++) {
		let cell = buildCell(entries[i][0], entries[i][1], entries[i][3]);
		grid.appendChild(cell);
	}

	return grid;
}

function buildAll () {
	let gridDivs = new Array();  // grid-level document objects
	let grids = new Array(); // individual grid data (sorted and ready for buildGrid)

	for (let i = 0; i < categories.length; i++) {
		grids[i] = new Array();
		gridDivs[i] = document.createElement('div');
		gridDivs[i].setAttribute('class', 'wrapper');
		
		let header = document.createElement('header');
		header.setAttribute('class', 'grid-header');
		let txt = document.createTextNode(categories[i][1]);
		header.appendChild(txt);
		gridDivs[i].appendChild(header);

		for (let x = 0; x < sites.length; x++) {
			if ( sites[x][2] === categories[i][0] ) {
				grids[i].push(sites[x]);
			}
		}
	}

	for (let i = 0; i < grids.length; i++) {
		gridDivs[i].appendChild( buildGrid(grids[i]) );
		let body = document.getElementById('container');
		body.appendChild(gridDivs[i]);
	}
}

buildAll()
