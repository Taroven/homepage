// Sites may be in any order you wish and will appear from top to bottom, left to right, within their categories.
// All sites require a category. Categories with no title (see below) will take up a little less vertical space since there's no text to act as a margin.
// Note: Custom favicons from files are not supported, but you can set a different URL to pull a favicon from (useful for sites on the local network).
// Format: ['Site Name', 'https://url.to.go.to/', 'category', 'http://optional.favicon.url']
export let sites = [
	['Google', 'https://www.google.com', 'general'],
	['Youtube', 'https://www.youtube.com/feed/subscriptions', 'media'],
	['Twitch', 'https://www.twitch.tv/directory/following/live', 'streamers'],
	['Plex', 'https://app.plex.tv/desktop/#!/', 'media'],
	['Homepage Source', 'https://gitlab.com/Taroven/homepage', 'code'],
	['Custom Site Icon', '127.0.0.1', 'general', 'https://www.bing.com']
]

// The category identifier (the first field) is used to sort sites into their desired spots.
// The category title may be any string you like. An blank string ('') may be used as a title.
export let categories = [
	['general', ''],
	['media', 'Media'],
	['streamers', 'Streamers'],
	['code', 'Coding']
]